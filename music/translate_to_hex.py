#!/usr/bin/env python3
import mido

sampling_freq = 96000
a = 440 # tuning
note_lookup = []
for i in range(128):
    note_lookup.append((a / 32) * (2 ** ((i - 9) / 12)))

def encode_period(note):
    freq = note_lookup[note]
    return round(sampling_freq / freq)
    
def tick_to_milis(time, tempo, ticks_per_beat):
    return int(mido.tick2second(time, tempo=tempo, ticks_per_beat=ticks_per_beat) * 1000)

def get_tempo(midi):
    tempo = 120
    for track in midi.tracks:
        for msg in track:
            if msg.type == "set_tempo":
                tempo = msg.tempo
                return tempo
    return tempo

def convert_track(track, ticks_per_beat, music_tempo):
    periods = []
    durations = []
    current_time = 0

    for msg in track:
        if msg.type not in ["note_on", "note_off"]:
            continue
        delta = tick_to_milis(msg.time, music_tempo, ticks_per_beat)
        if msg.velocity < 20:
            periods.append(1)
        else:
            periods.append(encode_period(msg.note))

        # we get notified at which point in time
        # events happen, from there infer duration.
        # -> delta between events is duration
        if len(durations) > 0:
            durations[-1] = delta
        durations.append(0)

    return periods, durations

def write_hex(filename, data):
    with open(filename, 'w') as f:
        for d in data:
            f.write("{0:04x}\n".format(d))

def main():
    filename = input("Midi input location: ")
    outfile_prefix = input("Output prefix: ")
    #filename = "Mario-Overworld.mid"
    #outfile_prefix = "MO"

    midi = mido.MidiFile(filename)
    tempo = get_tempo(midi)
    for i, track in enumerate(midi.tracks):
        periods, durations = convert_track(track, midi.ticks_per_beat, tempo)
        outfile_periods = outfile_prefix + str(i) + "Periods.hex"
        outfile_durations = outfile_prefix + str(i) + "Durations.hex"

        write_hex(outfile_periods, periods)
        write_hex(outfile_durations, durations)

        print("Add this to your main file and adapt to your liking: ")
        print("""
        melody_module #(
           .length({}),
           .period_location("music/{}"),
           .duration_location("music/{}")
        )	melody (
            .clk_i(clk_w),
            .reset_ni(BTN_N),
            .run_i(BTN1),
            .sample_o(sample_o),
            .sample_ready_o(sample_ready_w)
        );
        """.format(len(periods), outfile_periods, outfile_durations))

if __name__ == '__main__':
    main()
