module melody_module #(
   parameter length = 1,
   parameter period_location = "",
   parameter duration_location = ""
)	(
	input wire clk_i,
	input wire reset_ni,
	input wire run_i,
	output wire signed [23:0] sample_o,
	output wire sample_ready_o
);

wire run_w;
wire sample_request_w;
wire done_w;
wire [10:0] sound_period_w;
wire [12:0] sound_duration_w;
wire [10:0] address_w;

melody_ram #(
	.period_location(period_location),
	.duration_location(duration_location),
	.max_addr(length - 1)
) u_ram (
	.clk_i(clk_i),
	.reset_ni(reset_ni),
	.address_i(address_w),
	.sound_period_o(sound_period_w),
	.sound_duration_ms_o(sound_duration_w)
);

melody_counter #(
	// addrs start at zero, so max is length - 1
	.max_addr(length - 1)
) u_counter (
	.clk_i(clk_i),
	.run_i(run_i),
	.reset_ni(reset_ni),
	.sound_duration_ms_i(sound_duration_w),
	.address_o(address_w),
	.done_o(done_w)
);

melody_controller control_1 (
	.clk_i(clk_i),
	.reset_ni(reset_ni),
	.done_i(done_w),
	.restart_i(run_i),
	.run_o(run_w)
);

sample_requester u_sample_requester (
	.clk_i(clk_i),
	.reset_ni(reset_ni),
	.sample_request_o(sample_request_w)
);

sound_generator u_sound_generator (
	.clk_i(clk_i),
	.reset_ni(reset_ni),
	.sample_requested_i(sample_request_w),
	.sound_period_i(sound_period_w),
	.enable_i(run_w),
	.sample_ready_o(sample_ready_o),
	.sample_o(sample_o)
);

endmodule
