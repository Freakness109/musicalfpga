module i2s_dac(
	input wire clk_i,
	input wire reset_ni,
	output wire i2s_dac_mclk_o,
	output wire i2s_dac_sclk_o,
	output wire i2s_dac_lrclk_o,
	output wire i2s_dac_sdin_o,
	input wire signed [23:0] sample_r_i,
	input wire signed [23:0] sample_l_i,
	input wire sample_ready_i,
	output wire sample_request_o
);

parameter MCLK_TO_LRCLK = 'd384;
parameter MCLK_TO_SCLK = 'd6; //SCLK to LRCLK = 64 (as in datasheet)
parameter LEFT_CHANNEL = 1'b0;
parameter RIGHT_CHANNEL = 1'b1;

reg sclk_old_q;
reg sclk_q;
reg lrclk_old_q;
reg lrclk_q;
reg serial_out_d;
reg serial_out_q;
reg [8:0] lrclk_counter_q;
reg [2:0] sclk_counter_q;
reg [23:0] sample_r_q;
reg [23:0] sample_l_q;
reg [23:0] sample_r_d;
reg [23:0] sample_l_d;
reg [23:0] sample_r_buffered_q;
reg [23:0] sample_l_buffered_q;
reg sample_ready_old;

assign i2s_dac_mclk_o = clk_i; //the mclk is choosen to be the MCLK with 36.75MHz clk
assign i2s_dac_sclk_o = sclk_q;
assign i2s_dac_lrclk_o = lrclk_q;
assign i2s_dac_sdin_o = serial_out_q;
assign sample_request_o = lrclk_q;

//note:
//the device is operated in external serial mode
//LRCLK changes on falling sclk, LRCLK = 96kHz => MCLK/384
//sdin MSB is read at second rinsing edge of SCLK after LRCLK change

//buffer input sample_l_d
always @ (posedge clk_i, negedge reset_ni)
begin
	if(!reset_ni)
	begin	
	sample_r_buffered_q <= 'b0;
	sample_l_buffered_q <= 'b0;
	sample_ready_old <= 'b0;
end
else
begin
	sample_ready_old <= sample_ready_i;
	if (~sample_ready_old & sample_ready_i)
	begin
		sample_r_buffered_q <= sample_r_i;
		sample_l_buffered_q <= sample_l_i;			
	end
	else
	begin
		sample_r_buffered_q <= sample_r_buffered_q;
		sample_l_buffered_q <= sample_l_buffered_q;			
	end
end
end


//generate sclk
always @ (posedge clk_i, negedge reset_ni)
begin
	if(!reset_ni)
	begin
		sclk_q <= 1'b0;
		sclk_old_q <= 1'b0;
		sclk_counter_q <= (MCLK_TO_SCLK >> 1); //preload counter, shift sclk by 90 degree with respect to LRCLK
	end
	else	
	begin
		sclk_old_q <= sclk_q;
		if (sclk_counter_q == (MCLK_TO_SCLK >> 1) -1)
		begin
			sclk_counter_q <= sclk_counter_q + 1;
			sclk_q <= 1'b0;
		end
		else if (sclk_counter_q >= MCLK_TO_SCLK - 1)
		begin
			sclk_counter_q <= 'b0;
			sclk_q <= 1'b1;
		end	
		else
		begin
			sclk_counter_q <= sclk_counter_q + 1;
			sclk_q <= sclk_q;
		end	
	end
end

//generate lrclk
always @ (posedge clk_i, negedge reset_ni)
begin
	if(!reset_ni)
	begin
		lrclk_q <= LEFT_CHANNEL;
		lrclk_old_q <= RIGHT_CHANNEL;
		lrclk_counter_q <= 'b0;
	end
	else
	begin
		lrclk_old_q <= lrclk_q;
		if (lrclk_counter_q == (MCLK_TO_LRCLK >> 1) - 1)
		begin
			lrclk_counter_q <= lrclk_counter_q + 1;
			lrclk_q <= RIGHT_CHANNEL;
		end
		else if (lrclk_counter_q >= MCLK_TO_LRCLK - 1)
		begin
			lrclk_counter_q <= 'b0;
			lrclk_q <= LEFT_CHANNEL;
		end
		else
		begin
			lrclk_counter_q <= lrclk_counter_q + 1;
			lrclk_q <= lrclk_q;
		end	
	end
end


//statemachine for transmit data to DAC
always @ (posedge clk_i, negedge reset_ni)
begin
	if(!reset_ni)
	begin
		sample_r_q <= 'b0;
		sample_l_q <= 'b0;
		serial_out_q <= 1'b0;
	end
	else
	begin
		sample_r_q <= sample_r_d;
		sample_l_q <= sample_l_d;
		serial_out_q <= serial_out_d;
	end
end

always @ (*)
begin
	if(!reset_ni)
	begin
		sample_r_d = 'b0;
		sample_l_d = 'b0;
		serial_out_d = 1'b0;
	end
	else
	begin
		//state changes
		case(lrclk_q)
			LEFT_CHANNEL:
			begin
				if ((lrclk_q == LEFT_CHANNEL) & (lrclk_old_q == RIGHT_CHANNEL))
				begin
					//just entered left channel state
					sample_r_d = sample_r_buffered_q;
					sample_l_d = sample_l_buffered_q;
					serial_out_d = 1'b0;
				end
				else if ((sclk_old_q == 1'b1) & (sclk_q == 1'b0))
				begin
					sample_l_d = sample_l_q << 1;
					sample_r_d = sample_r_q;
					serial_out_d = sample_l_q[23];
				end		
				else			
				begin
					sample_l_d = sample_l_q;
					sample_r_d = sample_r_q;
					serial_out_d = serial_out_q;
				end		
			end

			RIGHT_CHANNEL:
			begin
				if ((lrclk_q == RIGHT_CHANNEL) & (lrclk_old_q == LEFT_CHANNEL))
				begin
					//just entered right channel state
					sample_r_d = sample_r_q;
					sample_l_d = sample_l_q;
					serial_out_d = 1'b0;
				end
				else if ((sclk_old_q == 1'b1) & (sclk_q == 1'b0))
				begin
					sample_l_d = sample_l_q;
					sample_r_d = sample_r_q << 1;
					serial_out_d = sample_r_q[23];
				end	
				else
				begin
					sample_l_d = sample_l_q;
					sample_r_d = sample_r_q;
					serial_out_d = serial_out_q;
				end		
			end

			default:
			begin
				sample_r_d = 'b0;
				sample_l_d = 'b0;
				serial_out_d = 1'b0;
			end
		endcase
	end
end



endmodule
