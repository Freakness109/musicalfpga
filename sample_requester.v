module sample_requester
#(
	// 2 * 96kHz (once per channel)
	// with base clock of 36.75MHz
	parameter request_every = 192
) (
	input wire clk_i,
	input wire reset_ni,
	output wire sample_request_o
);

reg sample_request_q;
assign sample_request_o = sample_request_q;

reg [8:0] counter_q;

always @ (posedge clk_i, negedge reset_ni)
begin
	if (!reset_ni)
	begin
		counter_q <= 0;
		sample_request_q <= 0;
	end
	else
	begin
		if (counter_q == request_every - 1)
		begin
			counter_q <= 0;
			sample_request_q <= 1;
		end
		else
		begin
			sample_request_q <= 0;
			counter_q <= counter_q + 1;
		end
	end
end
endmodule
