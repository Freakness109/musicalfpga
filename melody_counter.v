module melody_counter
(
	input wire clk_i,
	input wire reset_ni,
	input wire run_i,
	input wire [12:0] sound_duration_ms_i,
	output wire [10:0] address_o,
	output wire done_o
);

parameter max_addr = 511;

reg [15:0] ms_counter;
reg ms_tick;

// have 36.75 MHz at input
// --> count to 36750 to get
// 1 kHz
localparam ONE_MS = 36750;

always @ (posedge clk_i, negedge reset_ni)
begin
	if (!reset_ni)
	begin
		ms_counter <= 0;
		ms_tick <= 0;
	end
	else
	begin
		if (ms_counter >= ONE_MS - 1)
		begin
			ms_tick <= 1;
			ms_counter <= 0;
		end
		else
		begin
			ms_tick <= 0;
			ms_counter <= ms_counter + 1;
		end
	end
end

reg [10:0] cur_address;
assign address_o = cur_address;

reg [12:0] duration_counter;
reg ms_old;

reg running;
reg run_old;
reg done;
assign done_o = done;

always @ (posedge clk_i, negedge reset_ni)
begin
	if (!reset_ni)
	begin
		cur_address <= 0;
		duration_counter <= 0;
		ms_old <= 0;
		running <= 0;
		done <= 0;
	end
	else
	begin
		if (ms_tick && !ms_old && running)
		begin
			if (sound_duration_ms_i <= duration_counter)
			begin
				duration_counter <= 0;
				if (cur_address >= max_addr)
				begin
					cur_address <= 0;
					done <= 1;
					running <= 0;
				end
				else
				begin
					cur_address <= cur_address + 1;
				end
			end
			else
			begin
				duration_counter <= duration_counter + 1;
			end
		end
		else
		begin
			if (!run_old && run_i)
			begin
				running <= 1;
			end
			done <= 0;
		end
		run_old <= run_i;
		ms_old <= ms_tick;
	end
end

endmodule
