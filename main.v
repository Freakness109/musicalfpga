module main (
	input wire CLK,
	input wire BTN_N,
	input wire BTN1,

	// dac lines
	output wire P1A1,
	output wire P1A2,
	output wire P1A3,
	output wire P1A4

	/* would be needed for adc
	output wire P1A7,
	output wire P1A8,
	output wire P1A9,
	output wire P1A10
	*/
);

// at 36.75 MHz
wire clk_w;
wire signed [23:0] sample_i1;
wire signed [23:0] sample_i2;
wire signed [23:0] sample_i3;
wire signed [23:0] sample_o;

wire sample_ready_w;

pll u_pll (
  .clock_in(CLK),
  .clock_out(clk_w),
  .locked()
 ); 

i2s_dac u_i2s_dac (
	.clk_i(clk_w),
	.reset_ni(BTN_N),
	.i2s_dac_mclk_o(P1A1),
	.i2s_dac_sclk_o(P1A3),
	.i2s_dac_lrclk_o(P1A2),
	.i2s_dac_sdin_o(P1A4),
	.sample_r_i(sample_o),
	.sample_l_i(sample_o),
	.sample_ready_i(sample_ready_w)
);

assign sample_o = (sample_i1 + sample_i2 + sample_i3) / 4;

melody_module #(
	.length(1230),
	.period_location("music/MO0Periods.hex"),
	.duration_location("music/MO0Durations.hex")
)	track1 (
	.clk_i(clk_w),
	.reset_ni(BTN_N),
	.run_i(BTN1),
	.sample_o(sample_i1),
	.sample_ready_o(sample_ready_w)
);

melody_module #(
	.length(1184),
	.period_location("music/MO1Periods.hex"),
	.duration_location("music/MO1Durations.hex")
)	track2 (
	.clk_i(clk_w),
	.reset_ni(BTN_N),
	.run_i(BTN1),
	.sample_o(sample_i2),
	.sample_ready_o()
);

melody_module #(
	.length(1138),
	.period_location("music/MO2Periods.hex"),
	.duration_location("music/MO2Durations.hex")
)	track3 (
	.clk_i(clk_w),
	.reset_ni(BTN_N),
	.run_i(BTN1),
	.sample_o(sample_i3),
	.sample_ready_o()
);
endmodule
