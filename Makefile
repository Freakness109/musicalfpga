.PHONY: all build verify upload lint sim
all: build
	# nothing
	
build:
	apio build

verify:
	apio verify

upload:
	apio upload

lint:
	apio lint

sim:
	apio sim

clean:
	apio clean
