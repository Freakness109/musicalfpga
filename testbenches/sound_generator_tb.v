`timescale 1ns/1ps
module sound_generator_tb();

// input
reg clk_i, reset_ni;
reg [10:0] sound_period = 20;

// wires
wire sample_requested;

// output
wire sample_ready;
wire signed [23:0] sample;

sample_requester
#(
	.request_every(8)
) u_sample_requester (
	.clk_i(clk_i),
	.reset_ni(reset_ni),
	.sample_request_o(sample_requested)
);

sound_generator dut
(
	.clk_i(clk_i),
	.reset_ni(reset_ni),
	.sample_requested_i(sample_requested),
	.sound_period_i(sound_period),
	.sample_ready_o(sample_ready),
	.sample_o(sample)
);

// generate reset:
initial
begin
	reset_ni = 0;
	#3 reset_ni = 1;
end

// clock
initial
begin
	clk_i = 0;
end

always
begin
	#2 clk_i = !clk_i;
end

// simulate
parameter DURATION = 10000;
`define DUMPSTR(x) `"x.vcd`"
initial begin
	$dumpfile(`DUMPSTR(`VCD_OUTPUT));
	$dumpvars(0, sound_generator_tb);
	#(DURATION) $display("End of simulation");
	$finish;
end

endmodule
