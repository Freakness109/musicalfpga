`timescale 1ns/1ps
module sample_requester_tb();

// input
reg clk_i, reset_ni;

// output
wire sample_requested;

sample_requester
#(
	.request_every(8)
) dut (
	.clk_i(clk_i),
	.reset_ni(reset_ni),
	.sample_request_o(sample_requested)
);

// generate reset:
initial
begin
	reset_ni = 0;
	#3 reset_ni = 1;
end

// clock
initial
begin
	clk_i = 0;
end

always
begin
	#2 clk_i = !clk_i;
end

// simulate
parameter DURATION = 3000;
`define DUMPSTR(x) `"x.vcd`"
initial begin
  $dumpfile(`DUMPSTR(`VCD_OUTPUT));
  $dumpvars(0, sample_requester_tb);
   #(DURATION) $display("End of simulation");
  $finish;
end

endmodule
