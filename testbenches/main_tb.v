`timescale 1ns/1ps
module main_tb();

//applied stimuli
reg clk_i, reset_ni;

//results
wire i2s_dac_mclk_o;
wire i2s_dac_sclk_o;
wire i2s_dac_lrclk_o;
wire i2s_dac_sdin_o;

//instantiate the device under test	
main dut(
	.CLK(clk_i),
	.BTN_N(reset_ni),
	.P1A1(i2s_dac_mclk_o),
	.P1A2(i2s_dac_lrclk_o),
	.P1A3(i2s_dac_sclk_o),
	.P1A4(i2s_dac_sdin_o)
);

//generate reset
initial
	begin
		reset_ni = 1'b0;
	#3	reset_ni = 1'b1;
	end
		
//generate clk		
initial
	clk_i = 1'b0;
	
always
	begin
	#2 clk_i = ~clk_i;
	force main_tb.dut.u_pll.clock_out = clk_i;
	end
	
//handle simulation
parameter DURATION = 1000000; //times timescale
`define DUMPSTR(x) `"x.vcd`"
initial begin
  $dumpfile(`DUMPSTR(`VCD_OUTPUT));
  $dumpvars(0, main_tb);
   #(DURATION) $display("End of simulation");
  $finish;
end	
	
endmodule
