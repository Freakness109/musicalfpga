module melody_controller(
	input wire clk_i,
	input wire reset_ni,
	input wire done_i,
	input wire restart_i,
	output wire run_o
);

reg playing;
assign run_o = playing;

reg done_old;
reg restart_old;

always @ (posedge clk_i, negedge reset_ni)
begin
	if (!reset_ni)
	begin
		playing <= 0;
		done_old <= 0;
		restart_old <= 0;
	end
	else
	begin
		if (!done_old && done_i)
		begin
			playing <= 0;
		end
		if (!playing && !restart_old && restart_i)
		begin
			playing <= 1;
		end
		done_old <= done_i;
		restart_old <= restart_i;
	end
end

endmodule
