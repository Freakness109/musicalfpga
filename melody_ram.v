module melody_ram #(
	parameter period_location = "",
	parameter duration_location = "",
	parameter max_addr = 50
)	(
	input wire clk_i,
	input wire reset_ni,
	input wire [10:0] address_i,
	output wire [10:0] sound_period_o,
	output wire [12:0] sound_duration_ms_o
);


reg [15:0] period_array [0:max_addr];
reg [10:0] period_q;
assign sound_period_o = period_q;

reg [15:0] duration_array [0:max_addr];
reg [12:0] duration_q;
assign sound_duration_ms_o = duration_q;

always @(posedge clk_i)
begin
	period_q = period_array[address_i][10:0];
end
always @(posedge clk_i)
begin
	duration_q = duration_array[address_i][12:0];
end

initial
begin
	if (period_location != 0)
	begin
		// setup memory:
		$readmemh(period_location, period_array);
		$readmemh(duration_location, duration_array);
	end
end

endmodule
