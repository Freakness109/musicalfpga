module sound_generator
(
	input wire clk_i,
	input wire reset_ni,
	input wire sample_requested_i,
	input wire enable_i,
	// period of 1920 is 50 Hz
	input wire [10:0] sound_period_i,
	output wire sample_ready_o,
	output wire signed [23:0] sample_o
);

localparam amplitude = 1<<20;
assign sample_ready_o = sample_ready_q;
assign sample_o = sample_q;

reg [11:0] counter_q;
reg [23:0] sample_q;
reg sample_ready_q;
reg sample_requested_old;

always @ (posedge clk_i, negedge reset_ni)
begin
	if (!reset_ni)
	begin
		counter_q <= 0;
		sample_q <= 0;
		sample_ready_q <= 0;
		sample_requested_old <= 0;
	end
	else
	begin
		if (!sample_requested_old && sample_requested_i && enable_i)
		begin
			// shift bc. we have two samples per unit,
			// effectively doubling the needed period
			if (counter_q >= (sound_period_i << 1))
			begin
				counter_q <= 0;
				// is it negative?
				if (!sample_q[23])
				begin
					sample_q <= -amplitude;
				end
				else
				begin
					sample_q <= amplitude;
				end
			end
			else
			begin
				counter_q <= counter_q + 1;
			end
			sample_ready_q <= 1;
		end
		else
		begin
			sample_ready_q <= 0;
		end
		sample_requested_old <= sample_requested_i;
	end
end

endmodule
